<?php
$gender = array(
    0 => 'Nam',
    1 => 'Nữ'
);
$faculty = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'Khoa học vật liệu'
);


?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="form.css">
</head>

<body>
    <div id='backDiv'>
        <form method="POST" enctype="multipart/form-data" action="filledForm.php">
        <?php if (!empty($errors)): ?>
            <div id="error">
                <?php foreach ($errors as $error): ?>
                    <div><?php echo $error ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
            <!-- fill the name info  -->
            <div class="infoDiv" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên </label>
                <input id="input" class="h-100" type="text" name="name" >
            </div>

            <!-- choose genders -->
            <div class="infoDiv">
                <label id="label" class="h-100" for="gender"> Giới tính </label>
                <div id="radioBtn" >
                    <?php
                    foreach ($gender as $key=> $value) {
                    ?>
                        <input id="radioBtn" type="radio" name="gender" value=<?=$key?>>
                        <?php echo $value ?> 
                    <?php
                    }
                    ?>
                </div>
            </div>

            <!-- fill in faculty info -->
            <div class="infoDiv">
                <label id="label" class="h-100">Phân khoa </label>
                <select id="input" class="h-100" name="faculty">
                    <?php foreach ($faculty  as $key => $value) : ?>
                        <option value=<?=$key ?>>
                            <?php echo $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <!-- fill the DOB -->
            <div class="infoDiv">
                <label id="label" class="h-100" for="birthday">Ngày sinh </label>
                <td height = '40px'>
                    <input type='date' id="input" class="h-100" name="date" data-date="" data-date-format="DD MMMM YYYY">
                </td>
                <script>
                    $(document).ready(function() {
                        $("#input").datepicker({
                            format: 'dd-mm-yyyy'    
                        });
                    });
                </script>
            </div>
            <div class="infoDiv">
                <label id="labelH" class="h-100" for="address">Địa Chỉ </label>
                <input id="input" class="h-100" type="text" name="address">
            </div>
            
            <!-- fil in image -->
            <div class="infoDiv">
                <label id="labelH" class="h-100">Hình ảnh </label>
                <input id="img" class="h-100" type="file" name="image">
            </div>

            <div id="btnDiv">
                <button type="submit" class="btn btn-success" id="submitId" >Đăng ký</button>
            </div>
        </form>
    </div>
</body>
</html>