<?php
session_start();
$pdo = new PDO('mysql:host=localhost;port=3306;dbname=student','root', ''); 
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo '<pre>';
    // var_dump($_SESSION);
    // echo '</pre>';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $date = $_SESSION['date'];
    $date = str_replace('/', '-', $date);

    $statement = $pdo->prepare("INSERT INTO  student (name, gender ,faculty,birthday ,address,avatar)
        VALUE (:name,:gender,:faculty,:birthday,:address,:avatar)");
    $statement->bindValue(':name', $_SESSION['name']);
    $statement->bindValue(':gender', $_SESSION['gender']);
    $statement->bindValue(':faculty', $_SESSION['faculty']);
    $statement->bindValue(':birthday', $date);
    $statement->bindValue(':address', $_SESSION['address']);
    $statement->bindValue(':avatar', $_SESSION['image']);
    $statement->execute();
}
?>
<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="completeSubmit.css">
</head>

<body>
    <div id="notice">
        <p>Bạn đã đăng ký thành công sinh viên</p>
        <a href="form.php">Quay lại danh sách sinh viên</a>
    </div>
</body>

</html>