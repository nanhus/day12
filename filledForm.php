<?php
session_start();
$errors = [];
if (!$_POST['name']) {
    $errors[]='Hãy nhập tên.';
}

if(!isset($_POST['gender'])) {
    $errors[]='Hãy nhập giới tính.';
}

if (!$_POST['faculty']) {
    $errors[]='Hãy nhập Khoa.';
}

if (!$_POST['date']) {
    $errors[]='Hãy nhập ngày sinh.';
}

$image=$_FILES['image'] ?? null;

// echo '<pre>';
// var_dump($_FILES["image"]);
// echo '</pre>';

$imgType = array('image/jpeg','image/png','image/avif','image/webp','image/apng','image/svg+xml');

if(!in_array($image["type"],$imgType) and $image["name"]!=null) {
    $errors[]='Ảnh không đúng định dạng!';
}

if (empty($errors)) {
    if (!is_dir('upload/')) {
        mkdir('upload/');
    }

   
    if ($image["name"] != null) {
        $currentDate = date("YmdHis");
        $nameArr = str_split($image["name"], strrpos($image["name"], "."));

      
        $image["name"] = $nameArr[0] . "_" . $currentDate . "." . substr($nameArr[1], 1);
        $imagePath = 'upload/' . $image['name'];
        move_uploaded_file($image['tmp_name'], $imagePath);
    }

 
    $date = date_create($_POST['date']);
    $date = date_format($date, "Y/m/d");
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['gender'] = $_POST['gender'];
    $_SESSION['faculty'] = $_POST['faculty'];
    $_SESSION['date'] = $date;
    $_SESSION['address'] = $_POST['address'];
    $_SESSION['image'] = $imagePath;
}
    // echo '<pre>';
    // var_dump($_SESSION);
    // echo '</pre>';
if ($_SERVER['REQUEST_METHOD']=== 'POST') {
    $date_ = date_create($_POST['date']);
    $date_ = date_format($date_, "d/m/Y");
    $gender;
    if ($_POST['gender']== 0) {
        $gender= "Nam";
    } else {
        $gender="Nữ";
    }
    $faculty;
    if ($_POST['faculty']=='MAT') {
        $faculty='Khoa học máy tính';
    } else {
        $faculty='Khoa học dữ liệu';
    }
   
}
?>
<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="form.css">
</head>

<body>
    <div id='backDiv'>
        <form method="POST" enctype="multipart/form-data" action="completeSubmit.php">

            <div class="infoDiv" class="required-field">
                <label class="fillLabel" class="h-100" for="name">Họ và tên </label>
                <div class="info">
                    <?php echo $_SESSION['name'] ?>
                </div>
            </div>

       
            <div class="infoDiv">
                <label class="fillLabel" class="h-100" for="gender"> Giới tính </label>
                <div class="info">
                    <?php echo $gender ?>
                </div>    
            </div>

      
            <div class="infoDiv">
                <label class="fillLabel" class="h-100">Phân khoa </label>
                <div class="info">
                    <?php echo $faculty ?>
                </div> 
            </div>
            
     
            <div class="infoDiv">
                <label class="fillLabel" class="h-100" for="birthday">Ngày sinh </label>
                <div class="info">
                    <?php echo $date_ ?> 
                </div>
            </div>

            <div class="infoDiv">
                <label class="fillLabel" class="h-100" for="address">Địa Chỉ </label>
                <div class="info">
                    <?php echo $_SESSION['address'] ?>
                </div> 
            </div>

            <div class="infoDiv">
                <label class="fillLabel" class="h-100" >Hình ảnh </label>
                <img id="imgShow" src="<?php if($_SESSION['image']!='images/') {echo $_SESSION['image']; } ?>" class="thump-image" style="margin-left: 10%; height: 60px;">
            </div>

            <div id="cfmBtn">
                <button type="submit" class="btn btn-success" id="submitId" >Xác nhận</button>
            </div>
        </form>
    </div>

</body>

</html>

